import time

time0=time.time()

def chi(z,omegam=0.27,H=100):
  # import cosmology
  # c0=cosmology.Cosmo(H0=H,omega_m=omegam)
  # return c0.Dc(0.,z)
  from astropy import cosmology
  from astropy.cosmology import FlatLambdaCDM
  cosmo = FlatLambdaCDM(H0=70, Om0=0.27)
  return cosmo.comoving_distance(z).value

def in_hull(p, hull):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    from scipy.spatial import Delaunay
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)
    return hull.find_simplex(p)>=0

import numpy as np
#import sys
#sys.path.append('/net/ff/ph/1/jxy131230/destest')
#import src.catalog as catalog

data=np.genfromtxt('CFHTLenS_W1.tsv',dtype=None,names=['ra','dec','e1','e2','weight','fitclass','Z_B','m','c2','star_flag'])
# data=data[(data['fitclass']==0)&(data['star_flag']==0)&(data['weight']>0)]#&(0.4<=data['Z_B'])&(data['Z_B']<0.6)]
data=data[(data['fitclass']==0)&(data['star_flag']==0)&(data['weight']>0)&(0.4<=data['Z_B'])&(data['Z_B']<0.6)]
datar=chi(data['Z_B'])
print '# of data = ',len(datar)
DPiL=-600. # unit Mpc/h
DPiH=0.
print '\Delta\Pi in [ ',DPiL,', ',DPiH,' )'

import healpy as hp

p1=[ min(data['ra'])-0.01 , min(data['dec'])-0.01 ]
p2=[ max(data['ra'])+0.01 , max(data['dec'])+0.01 ]
#hpmax=hp.ang2pix(2**16, np.pi/2.-np.radians(p1[1]),np.radians(p1[0]), nest=False)
#hpmin=hp.ang2pix(2**16, np.pi/2.-np.radians(p2[1]),np.radians(p2[0]), nest=False)

#dec,ra=hp.pix2ang(2**16,np.arange(hpmax-hpmin)+hpmin,nest=False)
#dec=90.-dec*180./np.pi
#ra=ra*180./np.pi
#mask1=(dec>p1[1])&(dec<p2[1])&(ra>p1[0])&(ra<p2[0])
#pix=np.arange(hpmax-hpmin)[mask1]+hpmin
#ra=ra[mask1]
#dec=dec[mask1]

ra=data['ra']
dec=data['dec']
pix=hp.ang2pix(2**16, np.pi/2.-np.radians(dec),np.radians(ra), nest=False)

tmpphot=[]
with open('vipers_photo_pdr1_W1_edited.reg','r') as f:
  for line in f:
    tmpphot.append(np.fromstring(line, sep=' ').reshape(((1+line.count(' '))/2,2)))

pzpix=[]
for i in range(len(tmpphot)):
	pzpix=np.hstack( (pzpix, pix[in_hull(np.vstack((ra,dec)).T,tmpphot[i])]) )

mask2=np.in1d(pix,np.unique(pzpix),assume_unique=True)
mask=pix[~mask2]

data=data[~mask2]

print '# of data = ',len(data)

# ---- use mask healpix as random position ------
#maskdots=hp.pix2ang(2**16,mask)
#rand_dec=90-maskdots[0]/np.pi*180
#rand_ra=maskdots[1]/np.pi*180
#rand_dec=rand_dec[rand_ra<50]
#rand_ra=rand_ra[rand_ra<50]
# -------- randomize position --------------
rand_ra=p1[0]+np.random.random(1000000)*(p2[0]-p1[0])
rand_dec=p1[1]+np.random.random(1000000)*(p2[1]-p1[1])
randpix=hp.ang2pix(2**16, np.pi/2-np.radians(rand_dec), np.radians(rand_ra), nest=False)
mask3=np.in1d(randpix,mask,assume_unique=False)
rand_ra=rand_ra[mask3]
rand_dec=rand_dec[mask3]
print '# of rands = ',len(rand_ra)
# ---- ramdomly assign redshift ------------
rand_z=data['Z_B'][np.random.random_integers( 0 , len(data['Z_B'])-1 , len(rand_ra) )]
rand_r=chi(rand_z)

print '~~~~~~~~~~~~~~~~~~ pre-process finished time = ',time.time()-time0

import treecorr

dataE=treecorr.Catalog(g1=data['e1'], g2=data['e2']-data['c2'], w=data['weight'], ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
dataM=treecorr.Catalog(k=1+data['m'], w=data['weight'], ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
dataP=treecorr.Catalog(ra=data['ra'], dec=data['dec'], w=data['weight'], r=datar, ra_units='deg', dec_units='deg')
randP=treecorr.Catalog(ra=rand_ra, dec=rand_dec, r=rand_r, ra_units='deg', dec_units='deg')

ED=treecorr.NGCorrelation(nbins=10, min_sep=.3/.7, max_sep=100./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)
mD=treecorr.NKCorrelation(nbins=10, min_sep=.3/.7, max_sep=100./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)
ER=treecorr.NGCorrelation(nbins=10, min_sep=.3/.7, max_sep=100./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)
mR=treecorr.NKCorrelation(nbins=10, min_sep=.3/.7, max_sep=100./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)
RR=treecorr.NNCorrelation(nbins=10, min_sep=.3/.7, max_sep=100./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)

ED.process(dataP,dataE,metric='Rperp')
mD.process(dataP,dataM,metric='Rperp')
ER.process(randP,dataE,metric='Rperp')
mR.process(randP,dataM,metric='Rperp')
RR.process(randP,randP,metric='Rperp')

xi_gp=ED.xi/mD.xi-ER.xi/mR.xi
#xi_gp=(ED.xi/mD.xi*ED.npairs-ER.xi/mR.xi*ER.npairs)/RR.npairs
wgp=xi_gp*(DPiH-DPiL)
error_wgp=np.sqrt( (ED.varxi/ED.xi**2 + mD.varxi/mD.xi**2) * (ED.xi/mD.xi)**2 + (ER.varxi/ER.xi**2 + mR.varxi/mR.xi**2) * (ER.xi/mR.xi)**2 ) *(DPiH-DPiL)
#error_wgp=np.sqrt( ED.varxi*(ED.npairs/RR.npairs)**2 + ER.varxi*(ER.npairs/RR.npairs)**2 )*120
detection=np.sqrt( sum((wgp/error_wgp)**2) /9 )

xi_gx=ED.xi_im/mD.xi-ER.xi_im/mR.xi
wgx=xi_gx*(DPiH-DPiL)
error_wgx=np.sqrt( (ED.varxi/ED.xi_im**2 + mD.varxi/mD.xi**2) * (ED.xi_im/mD.xi)**2 + (ER.varxi/ER.xi_im**2 + mR.varxi/mR.xi**2) * (ER.xi_im/mR.xi)**2 ) *(DPiH-DPiL)

print 'wgp = ',wgp
print 'error_wgp = ',error_wgp
print 'mean r = ',ED.meanr
print 'ED pairs N = ',ED.npairs
print 'detection = ',detection
print '~~~~~~~~~~~~~~~~~~~~~~~~ process finished time = ',time.time()-time0

np.savetxt('pycorr.out',(ED.meanr, ED.xi, mD.xi, ED.npairs, ER.xi, mR.xi, ER.npairs, RR.npairs, ED.varxi, ER.varxi))

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

fig1=plt.figure()
plt.semilogx()
plt.errorbar(ED.meanr,wgp*np.sqrt(ED.meanr),error_wgp*np.sqrt(ED.meanr),fmt='o')
plt.plot(ED.meanr,np.zeros(10),color='k')
plt.xlabel('$r_p$')
plt.ylabel('$w_{g+}\sqrt{r_p}$')
plt.savefig('pycorr_plus.png')

fig2=plt.figure()
plt.semilogx()
plt.errorbar(ED.meanr,wgx*np.sqrt(ED.meanr),error_wgx*np.sqrt(ED.meanr),fmt='o')
plt.plot(ED.meanr,np.zeros(10),color='k')
plt.xlabel('$r_p$')
plt.ylabel('$w_{gx}\sqrt{r_p}$')
plt.savefig('pycorr_cross.png')

fig3=plt.figure()
plt.plot(rand_ra,rand_dec,'.')
plt.savefig('rands.png')

fig4=plt.figure()
plt.plot(data['ra'],data['dec'],'.')
plt.savefig('data.png')

#fig5=plt.figure()
#for i in range(len(tmpphot)):
#	plt.plot( np.append(tmpphot[i].T[0],tmpphot[i].T[0][0]), np.append(tmpphot[i].T[1],tmpphot[i].T[1][0]),'-')
#plt.savefig('mask.pdf')
