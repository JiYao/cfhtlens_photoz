import fitsio as fio
import numpy as np

cfht=fio.FITS('cfht_vipers_shape_match.fits')[-1].read()

# nb still need to exclude z_flag==0,1 objects

cfht=cfht[((cfht['z_flag']==2)|(cfht['z_flag']==3)|(cfht['z_flag']==4)|(cfht['z_flag']==9))&(cfht['z_spec']>0)&(cfht['weight']>0)]




vipers1=fio.FITS('VIPERS_W1_SPECTRO_PDR1.fits.gz')[-1].read()

vipers4=fio.FITS('VIPERS_W4_SPECTRO_PDR1.fits.gz')[-1].read()

vipers=np.append(vipers1,vipers4)

vipers['zflg']=np.floor(vipers['zflg'])

vipers=vipers[((vipers['zflg']==2)|(vipers['zflg']==3)|(vipers['zflg']==4)|(vipers['zflg']==9))&(vipers['zspec']>0)]




mask=np.load('wfirstvipersmask.npy')

#limit to joint photo-spec mask

import healpy as hp

cpix=hp.ang2pix(4096, np.pi/2.-np.radians(cfht['dec']),np.radians(cfht['ra']), nest=False)

vpix=hp.ang2pix(4096, np.pi/2.-np.radians(vipers['delta']),np.radians(vipers['alpha']), nest=False)

cmask=np.in1d(cpix,mask,assume_unique=False)

vmask=np.in1d(vpix,mask,assume_unique=False)

cfht=cfht[cmask]

vipers=vipers[vmask]




import scipy.special

csr=.5-.5*scipy.special.erf(10.8*(.44-vipers['zspec']))

vipersweight=(1./vipers['tsr'])*(1./vipers['ssr'])#*(1./csr)

vipers=vipers[(~np.isinf(vipersweight))&(vipersweight>0)]

vipersweight=vipersweight[(~np.isinf(vipersweight))&(vipersweight>0)]




h,z=np.histogram(vipers['zspec'],weights=vipersweight,bins=50)

z=(z[:-1]+z[1:])/2.

import scipy.interpolate as interp

f=interp.interp1d(z, h/np.sum(h), kind='cubic',bounds_error=False,fill_value=0.)




ran=fio.FITS('cfhtvipers_large_random.fits.gz')[-1].read()

# ranz=np.random.choice(np.arange(1000000)*2./1000000., size=len(ran), replace=True, p=f(np.arange(1000000)*2./1000000.)/np.sum(f(np.arange(1000000)*2./1000000.)))




# import kmeans_radec as km

# km0 = km.kmeans_sample(np.vstack((vipers['alpha'],vipers['delta'])).T, 50, maxiter=100, tol=1.0e-5)

# vipersreg=km0.labels

# cfhtreg=km0.find_nearest(np.vstack((cfht['ra'],cfht['dec'])).T)

# ranreg=km0.find_nearest(np.vstack((ran['ra'],ran['dec'])).T)




# cfhtr=chi(cfht['z_spec'])

# vipersr=chi(vipers['zspec'])

# ranr=chi(ranz)




# np.save('cfhtr.npy',cfhtr)

# np.save('vipersr.npy',vipersr)

# np.save('ranr.npy',ranr)




cfhtr=np.load('cfhtr.npy')

vipersr=np.load('vipersr.npy')

ranr=np.load('ranr.npy')




ranmask=np.random.choice(np.arange(len(ran)),1000000,replace=False)

ran=ran[ranmask]

ranr=ranr[ranmask]

# ranreg=ranreg[ranmask]




import treecorr




# cg=treecorr.Catalog(file_name='cg.fits',num=-1,ra_col='ra',dec_col='dec',r_col='r',w_col='w',g1_col='g1',g2_col='g2',ra_units='deg', dec_units='deg')

# vn=treecorr.Catalog(file_name='vn.fits',num=-1,ra_col='ra',dec_col='dec',r_col='r',w_col='w',ra_units='deg', dec_units='deg')

# de = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = 100, max_rpar = 6000, bin_slop=0.01, verbose=0)

# de2 = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = -60, max_rpar = 60, bin_slop=0.01, verbose=0)

# de.process(vn,cg,metric='Rperp')

# de2.process(vn,cg,metric='Rperp')




cg=treecorr.Catalog(g1=-cfht['e1'], g2=-(cfht['e2']-cfht['c2']), w=cfht['weight'], ra=cfht['ra'], dec=cfht['dec'],r=cfhtr, ra_units='deg', dec_units='deg')

ck=treecorr.Catalog(k=(1.+cfht['m']), w=cfht['weight'], ra=cfht['ra'], dec=cfht['dec'],r=cfhtr, ra_units='deg', dec_units='deg')

vn=treecorr.Catalog(ra=vipers['alpha'], dec=vipers['delta'], w=vipersweight,r=vipersr, ra_units='deg', dec_units='deg')

vr=treecorr.Catalog(ra=ran['ra'], dec=ran['dec'],r=ranr, ra_units='deg', dec_units='deg')




de = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = 100, max_rpar = 6000, bin_slop=0.01, verbose=0)

dm = treecorr.NKCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = 100, max_rpar = 6000, bin_slop=0.01, verbose=0)

re = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = 100, max_rpar = 6000, bin_slop=0.01, verbose=0)

rm = treecorr.NKCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = 100, max_rpar = 6000, bin_slop=0.01, verbose=0)

de2 = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = -60, max_rpar = 60, bin_slop=0.01, verbose=0)

dm2 = treecorr.NKCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = -60, max_rpar = 60, bin_slop=0.01, verbose=0)

re2 = treecorr.NGCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = -60, max_rpar = 60, bin_slop=0.01, verbose=0)

rm2 = treecorr.NKCorrelation(nbins=10, min_sep=.6, max_sep=60, min_rpar = -60, max_rpar = 60, bin_slop=0.01, verbose=0)




de.process(vn,cg,metric='Rperp')

dm.process(vn,ck,metric='Rperp')

re.process(vr,cg,metric='Rperp')

rm.process(vr,ck,metric='Rperp')

de2.process(vn,cg,metric='Rperp')

dm2.process(vn,ck,metric='Rperp')

re2.process(vr,cg,metric='Rperp')

rm2.process(vr,ck,metric='Rperp')




xip=de2.xi/dm2.xi-re2.xi_im/rm2.xi

xim=de2.xi_im/dm2.xi-re2.xi/rm2.xi

varxi=np.sqrt(de.varxi)

varxi2=np.sqrt(de2.varxi)

print xip*120,varxi

print xim*120*.1,varxi2
