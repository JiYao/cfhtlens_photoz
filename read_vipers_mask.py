import numpy as np
import healpy as hp

plg = [np.array(map(None, line.split())) for line in open('vipers_photo_pdr1_W1.reg')]

total=[]

Nplg=len(plg)
for i in range(Nplg):
	Ndot=len(plg[i])/2
	t=plg[i].reshape(Ndot,2)
	t=t.astype(np.float)
	#t=np.append(t,t[0])
	#t=t.reshape(Ndot+1,2)
	theta=np.pi/2-np.radians(t.T[1])
	phi=np.radians(t.T[0])
	z=np.cos(theta)
	x=np.sin(theta)*np.cos(phi)
	y=np.sin(theta)*np.sin(phi)
	r=np.append(x,y)
	r=np.append(r,z)
	r=r.reshape(3,Ndot)
	#rad=np.append(theta,phi)
	#rad=rad.reshape(2,Ndot)
	qp=hp.query_polygon(128,r)
	total=np.union1d(total,qp)
	print i,len(qp),len(total)
	
np.save('test.npy',total)