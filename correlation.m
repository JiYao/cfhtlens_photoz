%% correlation for merged catalog with fixed estimators (ED-ER)/RR, using Troxel's catalog
%% initial setting

clc; clear; close all;

H0=3000; % unit Mpc/h where h=0.7
Nbin=10; % here use 1..10 instead of 0..9 in Ishak summationChi.c
Nreg=21;

pairsize=60; % unit h^-1(Mpc)
RMIN=0.0001;
EFACT=log(pairsize/H0/RMIN)/Nbin;
dChiMAX=pairsize/H0; % 60/3000 in SDSS
R=1; % 0.87 in SDSS, haven't figured out for merged catalog yet

rp=H0*RMIN*exp((1:Nbin)*EFACT);

%myCluster=parcluster('local');
%myCluster.NumWorkers=8;
%saveProfile(myCluster);
%parpool(myCluster);

%% read catalogs and randoms

%cd('C:\Users\Ji\Desktop\Research\Limber integral Weak Lensing\Codes\MergeData\STILTS\');

% alpha delta e1 e2 z m c2 weight MASK fitclass star_flag selmag region r i
%   1    2    3  4  5 6  7   8     9      10       11       12     13  14 15
% ra dec e1 e2 z m c2 weight region for TroxelCata
% 1    2     3    4  5 6   7        8         9
% note e2 has NOT been calibrated

load Troxel_W1.mat; % data
merged=dataW1;

% ra dec z tsr ssr zflag photoMask
% 1     2   3  4    5       6           7

load VIPERS_W1.mat
dataNOe=VIPERS_W1;

load randW1_20k.mat;
randcata=randW1;

ND=length(merged(:,1)); % # of galaxies in data
NR=length(randcata(:,1)); % # of galaxies in randoms
NB=length(dataNOe(:,1)); % # of galaxies in background

alphaD=merged(:,1)/180*pi;
deltaD=merged(:,2)/180*pi;
alphaB=dataNOe(:,1)/180*pi; % background, galaxies without ellipticity data
deltaB=dataNOe(:,2)/180*pi;
alphaR=randcata(:,1)/180*pi;
deltaR=randcata(:,2)/180*pi;

z=merged(:,5);  % get line-of-sight distance for data
chiD(ND)=0;
syms x;
for i=1:ND
    chiD(i)=quadgk( @(x) (0.3*(1+x).^3+0.7).^-0.5 , 0, z(i));
end;
chiD=chiD';

z=dataNOe(:,5);  % get line-of-sight distance for data
chiD(NB)=0;
syms x;
for i=1:NB
    chiB(i)=quadgk( @(x) (0.3*(1+x).^3+0.7).^-0.5 , 0, z(i));
end;
chiB=chiB';

randass=randi([1 ND],NR,1);
chiR=chiD(randass);  % randomly assign line-of-sight distance to randoms

%e1=merged(:,3)./(1+merged(:,6));
%e2=(merged(:,4)-merged(:,7))./(1+merged(:,6));
e1=merged(:,3);
e2=merged(:,4)-merged(:,7);
m=merged(:,6); % calibrate m
w=merged(:,8); % lensfit weight

regD=merged(:,13);
regB=dataNOe(:,13);
regR=randcata(:,3);

%% calculate SpD, SxD, SpSp, SxSx

ra1=alphaD;
dec1=deltaD;

ra2=alphaB;
dec2=deltaB;

SpD(1:Nbin)=0;
ED(1:Nbin,1:Nreg)=0;

SxD(1:Nbin)=0;
E45D(1:Nbin,1:Nreg)=0;

SpSp(1:Nbin)=0;
EE(1:Nbin,1:Nreg)=0;

SxSx(1:Nbin)=0;
E45E45(1:Nbin,1:Nreg)=0;

Npairs(1:Nbin)=0; % # of pairs
avg1=Npairs; % average factor
avg1p=avg1; % avg1'
binpairs(1:Nbin,1:Nreg)=0;
avg2=binpairs;
avg2p=avg2;

ux1=cos(dec1).*cos(ra1); % get 3D unit vector for each galaxy to be rotated (i)
uy1=cos(dec1).*sin(ra1);
uz1=sin(dec1);

ux2=cos(dec2).*cos(ra2); % 3D unit vector for each target galaxy (j or slt)
uy2=cos(dec2).*sin(ra2);
uz2=sin(dec2);

REmatrix1=[ra1 dec1 e1 e2]; % matrix for rotellip.m
REmatrix2=[ra2 dec2];

%spmd;
for i=labindex:numlabs:ND
    cossep=ux2*ux1(i)+uy2*uy1(i)+uz2*uz1(i);
    sep12=0.5*(chiD(i)+chiD).*acos(cossep); % (zi+zj)/2*theta
    ibins=1+fix( log(sep12/RMIN) / EFACT );
    slt=find( chiD(i)-dChiMAX<chiD & chiD<chiD(i)+dChiMAX & 1<=ibins & ibins<=Nbin ); % within angle and redshift range
    [e1i e2i]=rotellip(REmatrix1,i,REmatrix2,slt);
%     [e1j e2j]=rotellip(REmatrix2,slt,REmatrix1,i);
    ibins=ibins(slt);
    
    ipairs=length(slt);
    for j=1:ipairs
        
        bin=ibins(j);
        
        avg1(bin)=avg1(bin)+w(i)*(1+m(i));
        avg1p(bin)=avg1p(bin)+w(i);
        Npairs(bin)=Npairs(bin)+1;
        
        SpD(bin)=SpD(bin)+e1i(j);
        SxD(bin)=SxD(bin)+e2i(j);
%         SpSp(bin)=SpSp(bin)+e1i(j)*e1j(j);
%         SxSx(bin)=SxSx(bin)+e2i(j)*e2j(j);
        
        jk=setdiff(1:Nreg,[regD(i),regB(slt(j))]);
        binpairs(bin,jk)=binpairs(bin,jk)+1;
        avg2(bin,jk)=avg2(bin,jk)+w(i)*(1+m(i));
        avg2p(bin,jk)=avg2p(bin,jk)+w(i);
        ED(bin,jk)=ED(bin,jk)+e1i(j);
        E45D(bin,jk)=E45D(bin,jk)+e2i(j);
%         EE(bin,jk)=EE(bin,jk)+e1i(j)*e1j(j);
%         E45E45(bin,jk)=E45E45(bin,jk)+e2i(j)*e2j(j);
        
    end;
end;
%end;

%SpD=gplus(SpD,1); SpD=SpD{1};
%SxD=gplus(SxD,1); SxD=SxD{1};
%ED=gplus(ED,1); ED=ED{1};
%E45D=gplus(E45D,1); E45D=E45D{1};
%avg1=gplus(avg1,1); avg1=avg1{1};
%avg1p=gplus(avg1p,1); avg1p=avg1p{1};
%avg2=gplus(avg2,1); avg2=avg2{1};
%avg2p=gplus(avg2p,1); avg2p=avg2p{1};
%Npairs=gplus(Npairs,1); Npairs=Npairs{1};
%binpairs=gplus(binpairs,1); binpairs=binpairs{1};

SpD=SpD./avg1.*avg1p;
SxD=SxD./avg1.*avg1p;
avg1./avg1p
ED=ED./avg2.*avg2p;
E45D=E45D./avg2.*avg2p;
% SpSp=SpSp/(2*R)^2;
% EE=EE/(2*R)^2;
% SxSx=SxSx/(2*R)^2;
% E45E45=E45E45/(2*R)^2;
DD=Npairs;
DDjk=binpairs;

%% calculate SpR, SxR

ra1=alphaD;
dec1=deltaD;

ra2=alphaR;
dec2=deltaR;

SpR(1:Nbin)=0;
ER(1:Nbin,1:Nreg)=0;

SxR(1:Nbin)=0;
E45R(1:Nbin,1:Nreg)=0;

Npairs(1:Nbin)=0;
avg1=Npairs;
avg1p=avg1;
binpairs(1:Nbin,1:Nreg)=0;
avg2=binpairs;
avg2p=avg2;

ux1=cos(dec1).*cos(ra1); % get 3D unit vector for each galaxy to be rotated (i)
uy1=cos(dec1).*sin(ra1);
uz1=sin(dec1);

ux2=cos(dec2).*cos(ra2); % 3D unit vector for each target galaxy (j or slt)
uy2=cos(dec2).*sin(ra2);
uz2=sin(dec2);

REmatrix1=[ra1 dec1 e1 e2]; % matrix for rotellip.m
REmatrix2=[ra2 dec2];

%spmd;
for i=labindex:numlabs:ND
    cossep=ux2*ux1(i)+uy2*uy1(i)+uz2*uz1(i);
    sep12=0.5*(chiD(i)+chiR).*acos(cossep); % (zi+zj)/2*theta
    ibins=1+fix( log(sep12/RMIN) / EFACT );
    slt=find( chiD(i)-dChiMAX<chiR & chiR<chiD(i)+dChiMAX & 1<=ibins & ibins<=Nbin ); % within angle and redshift range
    [e1i e2i]=rotellip(REmatrix1,i,REmatrix2,slt);
    ibins=ibins(slt);
    
    ipairs=length(slt);
    for j=1:ipairs
        
        bin=ibins(j);
        
        Npairs(bin)=Npairs(bin)+1;
        avg1(bin)=avg1(bin)+w(i)*(1+m(i));
        avg1p(bin)=avg1p(bin)+w(i);
        
        SpR(bin)=SpR(bin)+e1i(j);
        SxR(bin)=SxR(bin)+e2i(j);
        
        jk=setdiff(1:Nreg,[regD(i),regR(slt(j))]);
        binpairs(bin,jk)=binpairs(bin,jk)+1;
        avg2(bin,jk)=avg2(bin,jk)+w(i)*(1+m(i));
        avg2p(bin,jk)=avg2p(bin,jk)+w(i);
        ER(bin,jk)=ER(bin,jk)+e1i(j);
        E45R(bin,jk)=E45R(bin,jk)+e2i(j);
        
    end;
end;
%end;

%SpR=gplus(SpR,1); SpR=SpR{1}; % sum up results in each worker
%SxR=gplus(SxR,1); SxR=SxR{1};
%ER=gplus(ER,1); ER=ER{1};
%E45R=gplus(E45R,1); E45R=E45R{1};
%avg1=gplus(avg1,1); avg1=avg1{1};
%avg1p=gplus(avg1p,1); avg1p=avg1p{1};
%avg2=gplus(avg2,1); avg2=avg2{1};
%avg2p=gplus(avg2p,1); avg2p=avg2p{1};
%Npairs=gplus(Npairs,1); Npairs=Npairs{1};
%binpairs=gplus(binpairs,1); binpairs=binpairs{1};

SpR=SpR./avg1.*avg1p;
SxR=SxR./avg1.*avg1p;
avg1./avg1p
ER=ER./avg2.*avg2p;
E45R=E45R./avg2.*avg2p;
DR=Npairs;
DRjk=binpairs;

%% calculate RR

ra1=alphaR;
dec1=deltaR;

ra2=alphaR;
dec2=deltaR;

Npairs(1:Nbin)=0;
binpairs(1:Nbin,1:Nreg)=0;

ux1=cos(dec1).*cos(ra1); % get 3D unit vector for each galaxy to be rotated (i)
uy1=cos(dec1).*sin(ra1);
uz1=sin(dec1);

ux2=cos(dec2).*cos(ra2); % 3D unit vector for each target galaxy (j or slt)
uy2=cos(dec2).*sin(ra2);
uz2=sin(dec2);

%spmd;
for i=labindex:numlabs:NR
    cossep=ux2*ux1(i)+uy2*uy1(i)+uz2*uz1(i);
    sep12=0.5*(chiR(i)+chiR).*acos(cossep); % (zi+zj)/2*theta
    ibins=1+fix( log(sep12/RMIN) / EFACT );
    slt=find( chiR(i)-dChiMAX<chiR & chiR<chiR(i)+dChiMAX & 1<=ibins & ibins<=Nbin ); % within angle and redshift range
    ibins=ibins(slt);
    
    ipairs=length(slt);
    for j=1:ipairs
        
        bin=ibins(j);
        
        Npairs(bin)=Npairs(bin)+1;
        
        jk=setdiff(1:Nreg,[regR(i),regR(slt(j))]);
        binpairs(bin,jk)=binpairs(bin,jk)+1;
        
    end;
end;
%end;

%Npairs=gplus(Npairs,1); Npairs=Npairs{1};
%binpairs=gplus(binpairs,1); binpairs=binpairs{1};

RR=Npairs;
RRjk=binpairs;
regcount(Nreg)=0;
for i=1:Nreg
    regcount(i)=length( find(regR==i) ); % ngalaxies_reg in Ishak summationChiR.c
end;

%% calculate wgp, wgx, wpp, wxx

wgp=2*H0*dChiMAX*( SpD/(ND*NB)-SpR/(ND*NR) ) ./ (RR/(NR^2)); % with normalization
for i=1:Nbin
    sigma_wgp(i)=sqrt(sum(( (     (ED(i,:)./DDjk(i,:)*DD(i))/(ND*NB) - (ER(i,:)./DRjk(i,:)*DR(i))/(ND*NR)  ) ./ ( RRjk(i,:)./(NR-regcount)*NR/(NR^2) ) *2*H0*dChiMAX-wgp(i) ).^2 ));
end;

wgx=2*H0*dChiMAX*( SxD/(ND^2)-SxR/(ND*NR) ) ./ (RR/(NR^2)); % with normalization
for i=1:Nbin
    sigma_wgx(i)=sqrt(sum(( (     (E45D(i,:)./DDjk(i,:)*DD(i))/(ND*NB) - (E45R(i,:)./DRjk(i,:)*DR(i))/(ND*NR)  ) ./ ( RRjk(i,:)./(NR-regcount)*NR/(NR^2) ) *2*H0*dChiMAX-wgx(i) ).^2 ));
end;

% wpp=2*H0*dChiMAX*(SpSp/ND^2)./(RR/NR^2);
% for i=1:Nbin
%     sigma_wpp(i)=sqrt(sum((     (EE(i,:)./DDjk(i,:)*DD(i))/(ND^2)   ./ ( RRjk(i,:)./(NR-regcount)*NR/(NR^2) ) *2*H0*dChiMAX-wpp(i) ).^2 ));
% end;
% 
% wxx=2*H0*dChiMAX*(SxSx/ND^2)./(RR/NR^2);
% for i=1:Nbin
%     sigma_wxx(i)=sqrt(sum((     (E45E45(i,:)./DDjk(i,:)*DD(i))/(ND^2)   ./ ( RRjk(i,:)./(NR-regcount)*NR/(NR^2) ) *2*H0*dChiMAX-wxx(i) ).^2 ));
% end;

%% results

chi2_gp=sum((wgp./sigma_wgp)).^2; % chi^2
detection=( chi2_gp/(Nbin-1) )^0.5; % how many sigma

save 'result.mat' rp wgp sigma_wgp wgx sigma_wgx chi2_gp detection;
save workspace;

%% plot

figure;
subplot(2,2,1);
errorbar(rp,wgp.*sqrt(rp),sigma_wgp.*sqrt(rp),'k.');
set(gca,'xscale','log');
xlabel('rp');
ylabel('sqrt(rp)*wg+');
hold on;
plot([0.1 100],[0 0]);
hold off;

subplot(2,2,2);
errorbar(rp,wgx.*sqrt(rp),sigma_wgx.*sqrt(rp),'k.');
set(gca,'xscale','log');
xlabel('rp');
ylabel('sqrt(rp)*wgx');
hold on;
plot([0.1 100],[0 0]);
hold off;

% subplot(2,2,3);
% errorbar(rp,wpp.*rp,sigma_wpp.*rp,'k.');
% set(gca,'xscale','log');
% xlabel('rp');
% ylabel('rp*w++');
% hold on;
% plot([0.1 100],[0 0]);
% hold off;
% 
% subplot(2,2,4);
% errorbar(rp,wxx.*rp,sigma_wxx.*rp,'k.');
% set(gca,'xscale','log');
% xlabel('rp');
% ylabel('rp*wxx');
% hold on;
% plot([0.1 100],[0 0]);
% hold off;

saveas(gcf,'W1_12k_20k','eps');

%delete(gcp('nocreate'));
