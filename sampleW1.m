%% select sample from CFHTLenS_W1

n=5000;

load CFHTLenS_W1.mat;

x=randi([1 length(CFHTLenS_W1(:,1))],1,n);
sample=CFHTLenS_W1(x,:);
plot(sample(:,1),sample(:,2),'.');
save 'sample.mat' sample;