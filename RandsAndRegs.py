def chi(z,omegam=0.27,H=100):
  # import cosmology  
  # c0=cosmology.Cosmo(H0=H,omega_m=omegam)
  # return c0.Dc(0.,z)
  from astropy import cosmology
  from astropy.cosmology import FlatLambdaCDM
  cosmo = FlatLambdaCDM(H0=70, Om0=0.27)
  return cosmo.comoving_distance(z).value

import numpy as np
import sys
sys.path.append('/net/ff/ph/1/jxy131230/destest')
import src.catalog as catalog
import fitsio as fio

w1=np.genfromtxt('/net/ff/ph/1/jxy131230/STILTS/photoz/match_W1_2.txt',dtype=None,names=['id','pos1','pos2','pos3','pos4','ALPHA_J2000','DELTA_J2000','id_IAU1','id_IAU2','num','alpha','delta','selmag','errselmag','pointing','quadrant','zflg','zspec','epoch','photoMask','tsr','ssr','Separation'])
#w4=np.genfromtxt('/share/des/disc2/cfhtlens/match_W4_2.txt',dtype=None,names=['ra','dec','e1','e2','weight','fitclass','z','m','c2','starflag'])
cfht=np.genfromtxt('/net/ff/ph/1/jxy131230/STILTS/photoz/ellipfull_mask0_WLpass_Wall_Zcut.tsv',names=['ra', 'dec','z','e1','e2','weight','m','c2','field'],dtype=None)

cfhtradec=cfht['ra']*cfht['dec']
w1radec=w1['ALPHA_J2000']*w1['DELTA_J2000']
#w4radec=w4['ALPHA_J2000']*w4['DELTA_J2000']

x1,y1=catalog.CatalogMethods.sort2(cfhtradec,w1radec)
#x4,y4=catalog.CatalogMethods.sort2(cfhtradec,w4radec)

cfhtzspec=np.zeros(len(cfht))
cfhtzspecflag=np.zeros(len(cfht))
cfhtzspec[x1]=w1['zspec'][y1]
cfhtzspecflag[x1]=w1['zflg'][y1]
#cfhtzspec[x4]=w4['zspec'][y4]
#cfhtzspecflag[x4]=w4['zflg'][y4]

#mask=np.append(x1,x4)
mask=x1

image=np.ones(len(mask), dtype=cfht.dtype.descr + [('z_spec','f8')] + [('z_flag','f8')])

for name in cfht.dtype.names:
  image[name]=cfht[name][mask]

image['z_spec']=cfhtzspec[mask]
image['z_flag']=np.floor(cfhtzspecflag[mask])

fits = fio.FITS('/net/ff/ph/1/jxy131230/STILTS/photoz/cfht_vipers_shape_match.fits','rw')
fits.write(image,clobber=True)
fits.close()

cfht=fio.FITS('/net/ff/ph/1/jxy131230/STILTS/photoz/cfht_vipers_shape_match.fits')[-1].read()

    # nb still need to exclude z_flag==0,1 objects
cfht=cfht[((cfht['z_flag']==2)|(cfht['z_flag']==3)|(cfht['z_flag']==4)|(cfht['z_flag']==9))&(cfht['z_spec']>0)&(cfht['weight']>0)]

vipers1=fio.FITS('/net/ff/ph/1/jxy131230/STILTS/photoz/VIPERS_W1_SPECTRO_PDR1.fits')[-1].read()
    #vipers4=fio.FITS('/net/ff/ph/1/jxy131230/STILTS/photoz/VIPERS_W4_SPECTRO_PDR1.fits.gz')[-1].read()
    #vipers=np.append(vipers1,vipers4)
vipers=vipers1
vipers['zflg']=np.floor(vipers['zflg'])
vipers=vipers[((vipers['zflg']==2)|(vipers['zflg']==3)|(vipers['zflg']==4)|(vipers['zflg']==9))&(vipers['zspec']>0)]

mask=np.load('wfirstvipersmask.npy')
    #limit to joint photo-spec mask
import healpy as hp
cpix=hp.ang2pix(4096, np.pi/2.-np.radians(cfht['dec']),np.radians(cfht['ra']), nest=False)
vpix=hp.ang2pix(4096, np.pi/2.-np.radians(vipers['delta']),np.radians(vipers['alpha']), nest=False)
cmask=np.in1d(cpix,mask,assume_unique=False)
vmask=np.in1d(vpix,mask,assume_unique=False)
cfht=cfht[cmask]
vipers=vipers[vmask]

import scipy.special
csr=.5-.5*scipy.special.erf(10.8*(.44-vipers['zspec']))
vipersweight=(1./vipers['tsr'])*(1./vipers['ssr'])#*(1./csr)
vipers=vipers[(~np.isinf(vipersweight))&(vipersweight>0)]
vipersweight=vipersweight[(~np.isinf(vipersweight))&(vipersweight>0)]

h,z=np.histogram(vipers['zspec'],weights=vipersweight,bins=50)
z=(z[:-1]+z[1:])/2.
import scipy.interpolate as interp
f=interp.interp1d(z, h/np.sum(h), kind='cubic',bounds_error=False,fill_value=0.)

ran=fio.FITS('cfhtvipers_large_random.fits.gz')[-1].read()
    # ranz=np.random.choice(np.arange(1000000)*2./1000000., size=len(ran), replace=True, p=f(np.arange(1000000)*2./1000000.)/np.sum(f(np.arange(1000000)*2./1000000.)))

import kmeans_radec as km
km0 = km.kmeans_sample(np.vstack((vipers['alpha'],vipers['delta'])).T, 50, maxiter=100, tol=1.0e-5)
vipersreg=km0.labels
cfhtreg=km0.find_nearest(np.vstack((cfht['ra'],cfht['dec'])).T)
ranreg=km0.find_nearest(np.vstack((ran['ra'],ran['dec'])).T)

    # cfhtr=chi(cfht['z_spec'])
    # vipersr=chi(vipers['zspec'])
    # ranr=chi(ranz)

    # np.save('cfhtr.npy',cfhtr)
    # np.save('vipersr.npy',vipersr)
    # np.save('ranr.npy',ranr)

cfhtr=np.load('cfhtr.npy')
vipersr=np.load('vipersr.npy')
ranr=np.load('ranr.npy')

ranmask=np.random.choice(np.arange(len(ran)),100,replace=False)
ran=ran[ranmask]
ranr=ranr[ranmask]
ranreg=ranreg[ranmask]