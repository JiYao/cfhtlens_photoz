import time

time0=time.time()

def chi(z,omegam=0.27,H=100):
  # import cosmology  
  # c0=cosmology.Cosmo(H0=H,omega_m=omegam)
  # return c0.Dc(0.,z)
  from astropy import cosmology
  from astropy.cosmology import FlatLambdaCDM
  cosmo = FlatLambdaCDM(H0=70, Om0=0.27)
  return cosmo.comoving_distance(z).value

import numpy as np

data=np.genfromtxt('CFHTLenS_W1.tsv',dtype=None,names=['ra','dec','e1','e2','weight','fitclass','Z_B','m','c2','star_flag'])
data=data[(data['fitclass']==0)&(data['star_flag']==0)&(data['weight']>0)&(0.4<=data['Z_B'])&(data['Z_B']<0.6)]
datar=chi(data['Z_B'])
print '# of data = ',len(datar)
DPiL=-60. # unit Mpc/h
DPiH=60.
print '\Delta\Pi in [ ',DPiL,', ',DPiH,' )'



print '~~~~~~~~~~~~~~~~~~ pre-process finished time = ',time.time()-time0

import treecorr

dataE=treecorr.Catalog(g1=data['e1'], g2=data['e2']-data['c2'], w=data['weight'], ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
dataM=treecorr.Catalog(k=1+data['m'], w=data['weight'], ra=data['ra'], dec=data['dec'], r=datar, ra_units='deg', dec_units='deg')
dataP=treecorr.Catalog(ra=data['ra'], dec=data['dec'], w=data['weight'], r=datar, ra_units='deg', dec_units='deg')

ED=treecorr.NGCorrelation(nbins=10, min_sep=.3/.7, max_sep=60./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)
mD=treecorr.NKCorrelation(nbins=10, min_sep=.3/.7, max_sep=60./.7, bin_slop=0.01, verbose=0, min_rpar=DPiL/.7, max_rpar=DPiH/.7)

ED.process(dataP,dataE,metric='Rperp')
mD.process(dataP,dataM,metric='Rperp')

xi_gp=ED.xi/mD.xi
#xi_gp=(ED.xi/mD.xi*ED.npairs-ER.xi/mR.xi*ER.npairs)/RR.npairs
wgp=xi_gp*(DPiH-DPiL)
error_wgp=np.sqrt( (ED.varxi/ED.xi**2 + mD.varxi/mD.xi**2) * (ED.xi/mD.xi)**2  ) *(DPiH-DPiL)
#error_wgp=np.sqrt( ED.varxi*(ED.npairs/RR.npairs)**2 + ER.varxi*(ER.npairs/RR.npairs)**2 )*120
detection=np.sqrt( sum((wgp/error_wgp)**2) /9 )

print 'wgp = ',wgp
print 'error_wgp = ',error_wgp
print 'mean r = ',ED.meanr
print 'ED pairs N = ',ED.npairs
print 'detection = ',detection
print '~~~~~~~~~~~~~~~~~~~~~~~~ process finished time = ',time.time()-time0
