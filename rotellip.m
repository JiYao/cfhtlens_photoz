%version for merged data using estimator (ED-ER)/RR
% rotate i-th galaxy in sample-i towards j-th galaxy in sample-j
function [e1i,e2i]=rotellip(samplei,i,samplej,j)
% alpha delta e1 e2  z ac-av dc-dv zc-zv weight multimatch Chi region
%  1      2   3  4   5

% galaxy j line of sight direction
kj_x=cos(samplej(j,2)).*cos(samplej(j,1));
kj_y=cos(samplej(j,2)).*sin(samplej(j,1));
kj_z=sin(samplej(j,2));

% galaxy i delta direction
Di_x=-sin(samplei(i,2)).*cos(samplei(i,1));
Di_y=-sin(samplei(i,2)).*sin(samplei(i,1));
Di_z=cos(samplei(i,2));

% galaxy i alpha direction
Ai_x=-sin(samplei(i,1));
Ai_y=cos(samplei(i,1));
Ai_z=0;

% find angle difference between observer coordinates and o-i-j coordinates
px=kj_x.*Di_x + kj_y.*Di_y + kj_z.*Di_z;
py=kj_x.*Ai_x + kj_y.*Ai_y + kj_z.*Ai_z;
tdphi=2*atan(py./px);

% rotate e1i e2i towards j direction
e1i=samplei(i,3).*cos(tdphi) + samplei(i,4).*sin(tdphi); % a-axis pointing to j
e2i=samplei(i,4).*cos(tdphi) - samplei(i,3).*sin(tdphi); % b-axis pointing to j

